package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import JSci.maths.statistics.ChiSqrDistribution;


/**
 * Created by Pagotn on 19/2/2557.
 */
public class cal_chi extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cal_chi);

        final DecimalFormat dfx = new DecimalFormat("0.000#");
        final TextView ans = (TextView) findViewById(R.id.x_ans);

        final EditText df = (EditText) findViewById(R.id.editTextx_df);
        final EditText p = (EditText) findViewById(R.id.editTextx_p);

        final Toast toast = Toast.makeText(this, "กรอกข้อมูลไม่ครบค่ะ", Toast.LENGTH_LONG);

        Button cal = (Button) findViewById(R.id.calx);
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String xxx = df.getText().toString();
                String yyy = p.getText().toString();

                if (xxx.length() != 0 || yyy.length() != 0) {
                    double yyy2 = Double.parseDouble(yyy);
                    int xxx2 = Integer.parseInt(xxx);
                    ChiSqrDistribution x = new ChiSqrDistribution(xxx2);
                    double ans_x = x.cumulative(yyy2);
                    ans.setText("Chi-square = " + dfx.format(1 - ans_x));
                } else {

                    ans.setText("");
                    toast.show();
                }
            }
        });


        ImageButton back = (ImageButton) findViewById(R.id.imgback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                finish();
                java.lang.System.exit(0);
            }
        });

        ImageButton home = (ImageButton) findViewById(R.id.imghome);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cal_chi.this, MainActivity.class);
                startActivity(intent);
            }
        });

        ImageButton menux = (ImageButton) findViewById(R.id.imgmenu);
        registerForContextMenu(menux);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Menu");
        menu.add(0, v.getId(), 0, "View tables");
        menu.add(0, v.getId(), 0, "Calculate");
        menu.add(0, v.getId(), 0, "Learn more");


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "View tables") {
            function1(item.getItemId());
        } else if (item.getTitle() == "Calculate") {
            function2(item.getItemId());
        } else if (item.getTitle() == "Learn more") {
            function3(item.getItemId());
        } else {
            return false;
        }
        return true;
    }

    public void function1(int id) {
        Intent i = new Intent(this, main_view.class);
        startActivity(i);
    }

    public void function2(int id) {
        Intent i = new Intent(this, main_cal.class);
        startActivity(i);
    }

    public void function3(int id) {
        Intent i = new Intent(this, main_learn.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, main_cal.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

}
