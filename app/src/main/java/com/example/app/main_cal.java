package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;

/**
 * Created by Pagotn on 18/2/2557.
 */
public class main_cal extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cal_main);

        ImageButton btn_calz = (ImageButton) findViewById(R.id.img_cal_z);
        btn_calz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(main_cal.this, cal_z.class);
                startActivity(i);
            }
        });

        ImageButton btn_calt = (ImageButton) findViewById(R.id.img_cal_t);
        btn_calt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(main_cal.this, cal_t.class);
                startActivity(i);
            }
        });

        ImageButton btn_calchi = (ImageButton) findViewById(R.id.img_cal_chi);
        btn_calchi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(main_cal.this, cal_chi.class);
                startActivity(i);
            }
        });

        ImageButton btn_calf = (ImageButton) findViewById(R.id.img_cal_f);
        btn_calf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(main_cal.this, cal_f.class);
                startActivity(i);
            }
        });


        ImageButton back = (ImageButton) findViewById(R.id.imgback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                finish();
                java.lang.System.exit(0);
            }
        });

        ImageButton home = (ImageButton) findViewById(R.id.imghome);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(main_cal.this, MainActivity.class);
                startActivity(intent);
            }
        });

        ImageButton menux = (ImageButton) findViewById(R.id.imgmenu);
        registerForContextMenu(menux);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Menu");
        menu.add(0, v.getId(), 0, "View tables");
        menu.add(0, v.getId(), 0, "Calculate");
        menu.add(0, v.getId(), 0, "Learn more");


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="View tables"){function1(item.getItemId());}
        else if(item.getTitle()=="Calculate"){function2(item.getItemId());}
        else if(item.getTitle()=="Learn more"){function3(item.getItemId());}
        else {return false;}
        return true;
    }

    public void function1(int id){
        Intent i = new Intent(this, main_view.class);
        startActivity(i);
    }
    public void function2(int id){
        Intent i = new Intent(this, main_cal.class);
        startActivity(i);
    }
    public void function3(int id){
        Intent i = new Intent(this, main_learn.class);
        startActivity(i);
    }
}
