package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import JSci.maths.statistics.NormalDistribution;

/**
 * Created by Pagotn on 18/2/2557.
 */
public class cal_z extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cal_z);

        final DecimalFormat dfx = new DecimalFormat("0.0000");
        final TextView ans = (TextView) findViewById(R.id.normal_ans);

        final EditText zsocre = (EditText) findViewById(R.id.editTextnormal);

        final Toast toast = Toast.makeText(this, "กรอกข้อมูลไม่ครบค่ะ", Toast.LENGTH_LONG);
        Button cal = (Button) findViewById(R.id.cal);
        cal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String z = zsocre.getText().toString();
                if (z.length() != 0) {

                    NormalDistribution xxx = new NormalDistribution();
                    double z2 = Double.parseDouble(z);
                    double zzz = xxx.cumulative(z2);

                    ans.setText("Area = " + dfx.format(zzz));
                } else {
                    toast.show();ans.setText("");
                }
            }
        });


        ImageButton back = (ImageButton) findViewById(R.id.imgback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                finish();
                java.lang.System.exit(0);
            }
        });

        ImageButton home = (ImageButton) findViewById(R.id.imghome);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(cal_z.this, MainActivity.class);
                startActivity(intent);
            }
        });

        ImageButton menux = (ImageButton) findViewById(R.id.imgmenu);
        registerForContextMenu(menux);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Menu");
        menu.add(0, v.getId(), 0, "View tables");
        menu.add(0, v.getId(), 0, "Calculate");
        menu.add(0, v.getId(), 0, "Learn more");


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "View tables") {
            function1(item.getItemId());
        } else if (item.getTitle() == "Calculate") {
            function2(item.getItemId());
        } else if (item.getTitle() == "Learn more") {
            function3(item.getItemId());
        } else {
            return false;
        }
        return true;
    }

    public void function1(int id) {
        Intent i = new Intent(this, main_view.class);
        startActivity(i);
    }

    public void function2(int id) {
        Intent i = new Intent(this, main_cal.class);
        startActivity(i);
    }

    public void function3(int id) {
        Intent i = new Intent(this, main_learn.class);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, main_cal.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

}
