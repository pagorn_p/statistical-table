package com.example.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Pagotn on 19/2/2557.
 */
public class main_learn extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.learn_main);

        final File file01 = assetToFile("Learn_Z.pdf");
        final File file02 = assetToFile("Learn_t.pdf");
        final File file03 = assetToFile("Learn_X.pdf");
        final File file04 = assetToFile("Learn_F.pdf");


        ImageButton btn_calz = (ImageButton) findViewById(R.id.img_cal_z);
        btn_calz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String mimeType = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(MimeTypeMap
                                .getFileExtensionFromUrl(file01.getPath()));
                intent.setDataAndType(Uri.fromFile(file01), mimeType);
                startActivity(Intent.createChooser(intent, "Open file with"));
            }
        });

        ImageButton btn_calt = (ImageButton) findViewById(R.id.img_cal_t);
        btn_calt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String mimeType = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(MimeTypeMap
                                .getFileExtensionFromUrl(file02.getPath()));
                intent.setDataAndType(Uri.fromFile(file02), mimeType);
                startActivity(Intent.createChooser(intent, "Open file with"));
            }
        });

        ImageButton btn_calchi = (ImageButton) findViewById(R.id.img_cal_chi);
        btn_calchi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String mimeType = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(MimeTypeMap
                                .getFileExtensionFromUrl(file03.getPath()));
                intent.setDataAndType(Uri.fromFile(file03), mimeType);
                startActivity(Intent.createChooser(intent, "Open file with"));
            }
        });

        ImageButton btn_calf = (ImageButton) findViewById(R.id.img_cal_f);
        btn_calf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                String mimeType = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(MimeTypeMap
                                .getFileExtensionFromUrl(file04.getPath()));
                intent.setDataAndType(Uri.fromFile(file04), mimeType);
                startActivity(Intent.createChooser(intent, "Open file with"));
            }
        });


        ImageButton back = (ImageButton) findViewById(R.id.imgback);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                finish();
                java.lang.System.exit(0);
            }
        });

        ImageButton home = (ImageButton) findViewById(R.id.imghome);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(main_learn.this, MainActivity.class);
                startActivity(intent);
            }
        });

        ImageButton menux = (ImageButton) findViewById(R.id.imgmenu);
        registerForContextMenu(menux);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Menu");
        menu.add(0, v.getId(), 0, "View tables");
        menu.add(0, v.getId(), 0, "Calculate");
        menu.add(0, v.getId(), 0, "Learn more");


    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="View tables"){function1(item.getItemId());}
        else if(item.getTitle()=="Calculate"){function2(item.getItemId());}
        else if(item.getTitle()=="Learn more"){function3(item.getItemId());}
        else {return false;}
        return true;
    }

    public void function1(int id){
        Intent i = new Intent(this, main_view.class);
        startActivity(i);
    }
    public void function2(int id){
        Intent i = new Intent(this, main_cal.class);
        startActivity(i);
    }
    public void function3(int id){
        Intent i = new Intent(this, main_learn.class);
        startActivity(i);
    }

    public File assetToFile(String filePath) {
        new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                , "tmp").mkdir();
        String fileName = filePath.substring(filePath.lastIndexOf("/") + 1);
        File file = new File(Environment.getExternalStorageDirectory()
                , "tmp/" + fileName);

        try {
            InputStream is = getResources().getAssets().open(filePath);
            OutputStream out = new FileOutputStream(file);
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            out.write(buffer, 0, buffer.length);
            is.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }
}
