package com.example.app;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.view.Window;
import android.widget.ImageButton;

public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton btn_calz = (ImageButton) findViewById(R.id.img_view);
        btn_calz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, main_view.class);
                startActivity(i);
            }
        });

        ImageButton btn_calt = (ImageButton) findViewById(R.id.img_cal);
        btn_calt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, main_cal.class);
                startActivity(i);
            }
        });

        ImageButton btn_calchi = (ImageButton) findViewById(R.id.img_learn);
        btn_calchi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, main_learn.class);
                startActivity(i);
            }
        });

        ImageButton btn_calf = (ImageButton) findViewById(R.id.img_about);
        btn_calf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, main_about.class);
                startActivity(i);
            }
        });



    }
}
